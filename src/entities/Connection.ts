import { Entity, PrimaryColumn, CreateDateColumn, Column, ManyToOne, JoinColumn, UpdateDateColumn } from "typeorm"
import { v4 as uuid } from "uuid";
import { User } from "./User";

@Entity("connections")
class Connection {

    @PrimaryColumn()
    id: string;

    @Column()
    admin_id: string;

    @Column()
    socket_id: string;

    @CreateDateColumn()
    created_at: Date;

    @Column()
    user_id: string;

    @UpdateDateColumn()
    updated_at: Date;

    @JoinColumn({ name: "user_id" })
    @ManyToOne(() => User)
    user: User;

    constructor() {
        if (!this.id) {
            this.id = uuid();
        }
    }
}

export { Connection }