/*
-- MIGRATIONS --
Vão dar um histórico de tudo que foi criado no Banco de Dados.
Isso ajuda a manter os BDs atualizados.

ORDEM DE CRIAÇÃO
1- Migration (yarn typeorm migration:create -n CreateMessages, dai preenche a tabela e usa yarn typeorm migration:run)
2- Entities (Message.ts pro exemplo)
3- Repositories (MessagesRepository.ts)
4- Services (MessagesService.ts)
5- Controller (MessagesController.ts)
*/