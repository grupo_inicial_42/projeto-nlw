/*
--- API ---
A API serve basicamente pra fazer a comunicação entre o CLIENTE e o SERVIDOR, o caminho de ida e volta!
Cliente: HTML, CSS (FrontEnd e Mobile)
API: Requisição POST user.
Server: Baclend, Acesso ao BD, Envio de E-mail, Regras de Negócios, Autenticação.

FLUXOGRAMA GERAL:
CLIENTE (Requisição POST) -->
API (Entrega as Requisições) -->
SERVER (Processa as requisições) -->
API DEVOLVE O QUE FOI PROCESSADO DO SERVER PARA O CLIENTE (Resposta em JSON)

-- TYPESCRIPT --
Superset do JavaScript. Trabalha, por exemplo, com tipagem de informação, garantindo
assim consistência de tipos nos parâmetros.

interface DadosDoUsuario {
    id: string, nome: string, email: string;
}

function cadastrarUsuario( { email, id, nome } : DadosDoUsuario ) {
    console.log(id, nome, email)
}
class CadastrarAdministrador {
    cadastrarUsuario("haiuhz8346", "Dani", "daniele@rocketseat.team")
}
class CadastrarVendedor() {
    cadastrarUsuario("123565487", "Danilo", "danilo@rocketseat.team")
}

*/